/* 
GSR connection pins to Arduino microcontroller

Arduino           GSR

GND               GND
5V                VCC
A2                SIG

D13             RED LED

*/

/*
 GSR, standing for galvanic skin response, is a method of 
 measuring the electrical conductance of the skin. Strong 
 emotion can cause stimulus to your sympathetic nervous 
 system, resulting more sweat being secreted by the sweat 
 glands. Grove – GSR allows you to spot such strong emotions 
 by simple attaching two electrodes to two fingers on one hand,
 an interesting gear to create emotion related projects, like 
 sleep quality monitor. http://www.seeedstudio.com/wiki/Grove_-_GSR_Sensor
 */

const int LED=13;
const int GSR=A2;
int threshold=0;
int temp;

void setup(){
  long sum=0;
  Serial.begin(9600);
  pinMode(LED,OUTPUT);
  digitalWrite(LED,LOW);
  delay(1000);
  
  for(int i=0;i<500;i++)
  {
  int sensorValue=analogRead(GSR);
  sum += sensorValue;
  delay(5);
  }
  threshold = sum/500;
   Serial.print("threshold =");
   Serial.println(threshold);
  }
  


void loop(){
  int sensorValue=analogRead(GSR);
  Serial.print("sensorValue=");
  Serial.println(sensorValue);
  Serial.print("temp=");
  Serial.println(temp);
  
  if (abs(sensorValue - temp) > 19) {
    Serial.println("UP!");
    delay(1500);
  }
  
  temp = sensorValue;
  delay(100);

  /* temp = threshold - sensorValue;
  if(abs(temp)>60)
  {
    sensorValue=analogRead(GSR);
    temp = threshold - sensorValue;
    if(abs(temp)>60){
    digitalWrite(LED,HIGH);
    Serial.println("Emotion Changes Detected!");
    delay(500);
    digitalWrite(LED,LOW);
    delay(500);
  } 
 }*/
}

